pragma solidity ^0.4.24;

import "./ownable.sol";
import "./safeMath.sol";

contract TypeOfFlower is Ownable {
    
    using SafeMath for uint256;
    using SafeMath128 for uint128;
    
    struct FlowerType {
        string name;
        uint128 price;
        bool isAvailable;
        bool isDelete;
    }
    
    
    mapping (string => FlowerType) nameToType;
    
    modifier isNotRecorded(string _name) {
        FlowerType memory thisType = nameToType[_name];
        require(keccak256(abi.encodePacked(thisType.name)) == keccak256(""));
        require(thisType.price == 0);
        require(thisType.isAvailable == false);
        _;
    }
    
    modifier isNotDeleted(string _name) {
        require(!nameToType[_name].isDelete);
        _;
    }
    
    modifier availableInShop(string _name) {
        require(nameToType[_name].isAvailable);
        require(!nameToType[_name].isDelete);
        _;
    }
    
    modifier unavailbleInShop(string _name) {
        FlowerType memory thisType = nameToType[_name];
        require(keccak256(abi.encodePacked(thisType.name)) != keccak256(""));
        require(thisType.price != 0);
        require(thisType.isAvailable == false);
        require(thisType.isDelete == false);
        _;
    }
    
    modifier inShop(string _name) {
        FlowerType memory thisType = nameToType[_name];
        require(keccak256(abi.encodePacked(thisType.name)) != keccak256(""));
        require(thisType.price != 0);
        _;
    }
    
    modifier moreThanZero(uint128 price) {
        require(price > 0);
        _;
    }
    
    function _addNewFlowerType(string _name, uint128 _price) internal onlyOwner isNotRecorded(_name) moreThanZero(_price) {
        uint128 weiCurrency = 1 finney;
        uint128 calculatedPrice = _price.mul(weiCurrency);
        nameToType[_name] = FlowerType(_name,calculatedPrice,true,false);
    }
    
    function getflowerType(string flowerName) public view inShop(flowerName) returns(string myName, uint128 myPrice, bool isAvailable, bool isDelete) {
        myName = nameToType[flowerName].name;
        myPrice = nameToType[flowerName].price;
        isAvailable = nameToType[flowerName].isAvailable;
        isDelete = nameToType[flowerName].isDelete;
    }

    function changeFlowerPrice(string flowerName, uint128 newPrice) public onlyOwner moreThanZero(newPrice) availableInShop(flowerName) {
        uint128 weiCurrency = 1 finney;
        uint128 calculatedPrice = newPrice.mul(weiCurrency);
        nameToType[flowerName].price = calculatedPrice;
    }
    
    function enableBuying(string flowerName) public onlyOwner unavailbleInShop(flowerName) {
        nameToType[flowerName].isAvailable = true;
    }
    
    function unableBuying(string flowerName) public onlyOwner availableInShop(flowerName) {
        nameToType[flowerName].isAvailable = false;
    }
    
    function deleteFlowerType(string flowerName) public onlyOwner availableInShop(flowerName) {
        nameToType[flowerName].isAvailable = false;
        nameToType[flowerName].isDelete = true;
    }
}