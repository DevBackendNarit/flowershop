pragma solidity ^0.4.24;

import "./stockFlower.sol";
import "./erc721.sol";


contract flowerShop is stockFlower,ERC721{
    
    using SafeMath for uint256;
    using SafeMath128 for uint128;
    using SafeMath64 for uint64;
    using SafeMath32 for uint32;
    using SafeMath8 for uint8;
    
    
    uint64 private saleAmount;
    
    mapping (string => uint) totalSaleByType;
    
    
    constructor() public {
        saleAmount = 0;
    }
    
    
    modifier enoughValue(string flowerName, uint8 amount) {
        uint cost = nameToType[flowerName].price.mul(amount);
        require(msg.value == cost);
        _;
    }
    
    function transfer(address _to, uint32 _tokenId) private {
        flowers[_tokenId].fOwner = _to;
    }
    
    function ownerOf(uint32 _tokenId) public view returns (address) {
        return flowers[_tokenId].fOwner;
    }
    
    function addNewFlowerType(string flowerName, uint128 price) public onlyOwner { //need to set up total sale so we
        _addNewFlowerType(flowerName,price); //this fn is from flowerType.sol
        totalSaleByType[flowerName] = 0;
    }
    
    function getFlowerIdInStockAndRemove(string flowerName) private returns(uint32) {
        uint32 flowerId = inStockFlowerId[flowerName][0];
        
        uint _length = inStockFlowerId[flowerName].length;
        
        if(_length == 1) {
            delete inStockFlowerId[flowerName][0];
            inStockFlowerId[flowerName].length--;
        }
        else {
            inStockFlowerId[flowerName][0] = inStockFlowerId[flowerName][_length.sub(1)];
            delete inStockFlowerId[flowerName][_length.sub(1)];
            inStockFlowerId[flowerName].length--;
        }
        
        return flowerId;
    }

    function buyFlower(string flowerName, uint8 amount) external payable availableInShop(flowerName) isBuyable(flowerName, amount) enoughValue(flowerName, amount) {
        owner.transfer(msg.value);
        // flowers[flowerName].stockAmount -= amount; 
        for(uint i = 0 ; i < amount ; i = i.add(1)) {
            uint32 flowerId = getFlowerIdInStockAndRemove(flowerName);
            transfer(msg.sender, flowerId);
        }
        
        totalSaleByType[flowerName] = totalSaleByType[flowerName].add(amount);
    }
    
    function getSaleAmountByName(string flowerName) public view availableInShop(flowerName) returns(uint) {
        return totalSaleByType[flowerName];
    }
    
}
