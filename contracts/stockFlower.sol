pragma solidity ^0.4.24;

import "./flowerType.sol";

contract stockFlower is TypeOfFlower {
    
    using SafeMath for uint256;
    using SafeMath128 for uint128;
    using SafeMath32 for uint32;
    using SafeMath8 for uint8;
    
    
    uint32 public counterId;

    
    struct Flower{
        string flowerType;
        uint32 id;
        address fOwner;
    }
    
    mapping (uint32 => Flower) flowers;
    mapping (string => uint32[]) inStockFlowerId;
    
    constructor() public {
        counterId = 1;
        // flowers.push(Flower("", 0, owner));
    }
    
    modifier isBuyable(string flowerName, uint8 amount) {
        require(inStockFlowerId[flowerName].length >= amount);
        require(amount > 0);
        _;
    }
    
    modifier isRemain(string flowerName, uint amount) {
        require(inStockFlowerId[flowerName].length >= amount);
        require(amount > 0);
        _;
    }
    
    modifier beOwner(uint32 _id) {
        require(flowers[_id].fOwner == msg.sender);
        _;
    }
    
    modifier isNotMoreThan10(uint8 amount) {
        require(amount <= 10);
        _;
    }
    
    function addFlowerToShop(string flowerTypeName) public onlyOwner inShop(flowerTypeName) { 
        //this fn will add new flower to stock's shop only and it cause fOwner of new flower to be shop's address(contract deployer)
        
        // flowers.push(Flower(flowerTypeName, counterId, owner));
        flowers[counterId] = Flower(flowerTypeName, counterId, owner);
        inStockFlowerId[flowerTypeName].push(counterId);
        counterId = counterId.add(1);
    }
    
    function addFlowerToShop(string flowerTypeName, uint8 amount) public onlyOwner inShop(flowerTypeName) isNotMoreThan10(amount) { 
        //this fn will add new flower to stock's shop only and it cause fOwner of new flower to be shop's address(contract deployer)
        for(uint8 i = 0 ; i < amount ; i = i.add(1)){
            // flowers.push(Flower(flowerTypeName, counterId, owner));
            flowers[counterId] = Flower(flowerTypeName, counterId, owner);
            inStockFlowerId[flowerTypeName].push(counterId);
            counterId = counterId.add(1);
        }
    }
    
    function removeFlowerById(uint32 _id) public beOwner(_id) {
        removeIdFromStock(flowers[_id].flowerType, _id);
        delete flowers[_id];
    }
    
    function removeFlowerByType(string fType, uint amount) public onlyOwner inShop(fType) isRemain(fType, amount) {
        uint _length = inStockFlowerId[fType].length;
        uint32 tempId;
        for(uint i = 0 ; i < amount ; i = i.add(1)) {
            tempId = inStockFlowerId[fType][_length.sub(1)];
            delete flowers[tempId];
            delete inStockFlowerId[fType][_length.sub(1)];
            
            _length = _length.sub(1);
        }
        inStockFlowerId[fType].length = _length;  // less gas used
    }
    
    function removeAllFlowerInStockByType(string fType) public onlyOwner inShop(fType) isRemain(fType, 1) {
        uint _length = inStockFlowerId[fType].length;
        removeFlowerByType(fType, _length);
        unableBuying(fType);
        
    }
    
    function removeIdFromStock(string fType, uint32 _id) private beOwner(_id) {  // backend function to serch and remove _id in mapping inStockFlowerId
        uint _length = inStockFlowerId[fType].length;
        uint idIndex;
        for(uint i = 0 ; i < _length ; i = i.add(1)) {
            if(inStockFlowerId[fType][i] == _id) {
                idIndex = i;
                break;
            }
        }
        
        if(_length == 1) {
            delete inStockFlowerId[fType][0];
            inStockFlowerId[fType].length--;
        } 
        else if(_length == idIndex.add(1)) {
            delete inStockFlowerId[fType][idIndex];
            inStockFlowerId[fType].length--;
        }
        else {
            inStockFlowerId[fType][idIndex] = inStockFlowerId[fType][_length.sub(1)];
            delete inStockFlowerId[fType][_length.sub(1)];
            inStockFlowerId[fType].length--;
        }
    }
    
    
    function getEachTypeAmountInStock(string flowerTypeName) public view availableInShop(flowerTypeName) returns(uint) {
        return inStockFlowerId[flowerTypeName].length;
    }
    
    function getFlowerById (uint32 _id) public view returns(string flowerType, uint32 id, address fOwner) {
        Flower memory thisFlower = flowers[_id];
        flowerType = thisFlower.flowerType;
        id = thisFlower.id;
        fOwner = thisFlower.fOwner;
    }
    
}


// if((_length.sub(amount)) >= amount) {  // This case, the remain flowers is mort than removed flowers. It easy to manage stockId array //  (length-amount) >= amount
        //     uint32 tempId;
        //     for(i = 0 ; i < amount ; i = i.add(1)) {
        //         tempId = inStockFlowerId[fType][i];
        //         delete flowers[tempId];  // delete data
        //         inStockFlowerId[fType][i] = inStockFlowerId[fType][_length.sub(1)];
        //         delete inStockFlowerId[fType][_length.sub(1)]; // delete ID
                
        //         _length = _length.sub(1); // _length is local but need to equal inStockFlowerId[].length every loop time 
        //         // inStockFlowerId[fType].length--;   //will comment
        //     }
        //     inStockFlowerId[fType].length = _length;
        // }
        
        // else {
        //     uint remainAmount = _length.sub(amount);
        //     for(i = 0 ; i < remainAmount ; i = i.add(1)) { // This loop is replacing deleted ID by using last ID
        //         tempId = inStockFlowerId[fType][i];
        //         delete flowers[tempId];  // delete data
        //         inStockFlowerId[fType][i] = inStockFlowerId[fType][_length.sub(1)];
        //         delete inStockFlowerId[fType][_length.sub(1)]; // delete ID
                
        //         _length = _length.sub(1); 
        //         // inStockFlowerId[fType].length--;
        //     }
        //     // inStockFlowerId[fType].length = _length;
            
        //     for(i = 0 ; i < amount.sub(remainAmount) ; i = i.add(1)) { // This loop just delete last ID
        //         tempId = inStockFlowerId[fType][_length.sub(1)];
        //         delete flowers[tempId];
        //         delete inStockFlowerId[fType][_length.sub(1)];
                
        //         _length = _length.sub(1);
        //         inStockFlowerId[fType].length--;
        //     }
        //     inStockFlowerId[fType].length = _length;
        // }