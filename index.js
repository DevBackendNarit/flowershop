const Web3 = require("web3");
const fs = require('fs');
var abi;

deployer = '0xedc62aff71078ef250f46404e36bf2a7a7da3927'


fs.readFile('./abi.json', 'utf8', function (err, data) {
  if (err) throw err;
  abi = JSON.parse(data);
  web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));
  myContractAddress = '0x01c8e1d821c0b6020fb328677102d7c0472948a9'
  ShopContract = new web3.eth.Contract(abi, myContractAddress);

  // addFlowerToShop("a",5).then(()=>{
  //   console.log("success")
  // })

  // getFlowerAmount("a").then((data) => {
  //   console.log(data)
  // })

  // buyFlower("a", 4, "0x16743592fb6dFf8Bf80c78d0E87733c2cdf8dC90", "480").then(() => {
  //   getFlowerAmount("a").then((data) => {
  //     console.log(data)
  //   })
  // })

  // addNewFlowerType("top",220).then(()=>{
  //   getFlowerType("top").then((data) =>{
  //     console.log(data)
  //   })
  // })

  // removeFlowerByType("a",3).then(()=>{
  //     getFlowerAmount("a").then((data) => {
  //     console.log(data)
  //   })
  // })

  // getFlowerType("a").then((data) =>{
  //   console.log(data)
  // })
});

function addNewFlowerType(name, price) {
  return ShopContract.methods.addNewFlowerType(name, price)
  .send({from:deployer})
}

function addFlowerToShop (name, amount) {
  return ShopContract.methods.addFlowerToShop(name, amount)
  .send({from: deployer, gas:3000000})
}

function changeFlowerPrice (name, price) {
  return ShopContract.methods.addNewFlowerType(name, price)
  .send({from : deployer})
}

function buyFlower (name, amount, buyer, val) {
  return ShopContract.methods.buyFlower(name, amount)
  .send({from : buyer, value: web3.utils.toWei(val, "finney"),gas:3000000})
}

function removeFlowerByType (name, amount) {
  return ShopContract.methods.removeFlowerByType(name, amount)
  .send({from: deployer, gas:3000000})
}

function removeFlowerById (id) {
  return ShopContract.methods.removeFlowerById(id)
  .send({from: deployer, gas:3000000})
}

function deleteFlowerType (name) {
  return ShopContract.methods.deleteFlowerType(name)
  .send({from : deployer})
}

function enableBuying (name) {
  return ShopContract.methods.enableBuying(name)
  .send({from : deployer})
}

function unableBuying (name) {
  return ShopContract.methods.unableBuying(name)
  .send({from : deployer})
}

//call function

function getFlowerType (name) {
  return ShopContract.methods.getFlowerType(name).call()
}

function getFlowerById (id) {
  return ShopContract.methods.getFlowerById(id).call()
}

function getFlowerAmount (name) {
  return ShopContract.methods.getEachTypeAmountInStock(name).call()
}

function getSaleAmount (name) {
  return ShopContract.methods.getEachTypeAmountInStock(name).call()
}




